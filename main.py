import sys
import os
import numpy as np
from sklearn.metrics import confusion_matrix, roc_curve, auc, classification_report, accuracy_score, jaccard_score
import pandas as pd

import matplotlib as mpl 
from matplotlib import pyplot as plt

import tensorflow as tf 
from tensorflow.keras.layers import Input
from tensorflow.keras import models
from tensorflow.keras.layers import Dense, GlobalAveragePooling2D
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau

from Utils import ImageUtils, DatasetUtils, ModelUtils, EvalUtils

gpus = tf.config.list_physical_devices('GPU')
print(tf.__version__)
print('gpus: ', gpus)
print('python version: ', sys.version)

sys.path.append('./Utils')

"""
Define constants for model training and evaluation

"""
TRAIN_MODEL = True
EVALUATE_MODEL = False

IMAGE_HEIGHT = 299
IMAGE_WIDTH = 299
CHANNELS = 3
INPUT = Input(shape=(None, None, CHANNELS))
CLASSES = ["Normal", "Drusen", "CNV", "DME"]
SEG_LAYERS = [ 'exterior', 'ilm', 'rnfl', 'ipl', 'inl', 'opl', 'elm', 'pr1', 'pr2', 'rpe' ]
SEG_COLOURS = [0, 76, 78, 117, 178, 202, 211, 225, 242, 255]
BATCH_SIZE = 2
EPOCHS = 5
EARLY_STOP_PATIENCE = 10
DO_NORMALISE = True
DO_IMAGE_AUGMENT = True
# MODELS = ('XCEPTION', 'VGG16', 'RESNET50V2', 'MOBILENETV2', 'INCEPTIONV3', 'CUSTOM')
MODELNAME = 'XCEPTION'

# model/history/tensorboard logs locations
OUTPUT_MODEL_LOCATION = f'./Models/{MODELNAME}/model.h5'
OUTPUT_HISTORY_LOCATION = f'./Models/{MODELNAME}/History/history.csv'
TB_LOG_LOCATION = './Logs'

""" 
Custom lookup dictionary definition for classification task
"""
lookupTableClass = tf.lookup.StaticHashTable(
    initializer=tf.lookup.KeyValueTensorInitializer(
        keys=tf.constant(CLASSES),
        values=tf.constant([0, 1, 2, 3])
    ),
    default_value=tf.constant(0)
)

""" 
Custom lookup dictionary definition for segmentation task
"""
lookupTableSeg = tf.lookup.StaticHashTable(
    initializer=tf.lookup.KeyValueTensorInitializer(
        keys=tf.constant(SEG_COLOURS),
        values=tf.constant([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    ),
    default_value=tf.constant(0)
)

def process_path_class(file_path):
    """ PROCESS A FILE PATH FOR THE DATASET.

    Files reside in directory struct:
      /data/train/images/class/filename
      /data/train/masks/class/filename

    Parameters:
        filePath (str):path to an image file
    Returns:
        (tuple):an input image and output mask
    """

    img = ImageUtils.load_image(file_path, IMAGE_HEIGHT, IMAGE_WIDTH, CHANNELS, DO_NORMALISE)

    rand_uniform = tf.random.uniform(shape=[], minval=0, maxval=4, dtype=tf.int32)
    
    if DO_IMAGE_AUGMENT :
        img = ImageUtils.augmentImage(img, (IMAGE_HEIGHT, IMAGE_WIDTH), rand_uniform)

    parts = tf.strings.split(file_path, os.path.sep)
    label = parts[-2]

    index = lookupTableClass.lookup(label)
    one_hot = tf.one_hot(index, len(CLASSES))

    return img, one_hot

def process_path_combine_image(file_path):
    """
    PROCESS A FILE PATH FOR THE DATASET
    /data/class/images/filename
    /data/class/masks/filename
    input (str) - path to an image file
    output (tuple)- an input image and output mask
    """

    mask_path = tf.strings.regex_replace(file_path, 'images', 'masks', replace_global=False)
    mask_path = tf.strings.regex_replace(mask_path, '.png', '-mask.png', replace_global=False)

    # load the image and mask and convert and contact
    mask_channels = 1
    img = ImageUtils.load_image(file_path, IMAGE_HEIGHT, IMAGE_WIDTH, mask_channels, DO_NORMALISE)    
    mask = ImageUtils.load_image(file_path, IMAGE_HEIGHT, IMAGE_WIDTH, mask_channels, DO_NORMALISE)    
    img = tf.concat((img, img, mask), -1)

    # grab the classification
    parts = tf.strings.split(file_path, os.path.sep)
    label = parts[-3]
    index = lookupTableClass.lookup(label)
    label = tf.one_hot(index, len(CLASSES))

    return img, label

def do_training(train_dataset, valid_dataset):
    """
    TRAIN MODEL ON THE DATASETS
    train_dataset (tf.dataset) - the training dataset ready for consumption
    valid_dataset (tf.dataset) - the validation dataset ready for consumption
    """

    train_data_set_size = DatasetUtils.get_size(train_dataset)
    valid_data_set_size = DatasetUtils.get_size(valid_dataset)
    steps_per_epoch = int(train_data_set_size // BATCH_SIZE)
    validation_steps = int(valid_data_set_size // BATCH_SIZE)

    ModelUtils.prepareModelOutputLocation(OUTPUT_MODEL_LOCATION)

    if ModelUtils.modelExists(OUTPUT_MODEL_LOCATION) :
        model = ModelUtils.loadModel(OUTPUT_MODEL_LOCATION, compile=False)
    else:
        model = ModelUtils.createModel(MODELNAME, include_top=False, weights='imagenet')
        model.trainable = False 

        # add a global spatial average pooling layer
        x = model.output
        x = GlobalAveragePooling2D(name='gap_addon')(x)

        # add a fully connected layer
        x = Dense(1024, activation='relu', name='relu_addon')(x)

        # and add a logistic layer -- we have nbr_classes
        predictions = Dense(len(CLASSES), activation='softmax', name='dense_addon')(x)

        # define model to train
        model = models.Model(inputs=model.input, outputs=predictions)
        
        # train for a few epochs to adjust the top layers
        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=[ 'accuracy', 'top_k_categorical_accuracy' ])
        model.fit(train_dataset, epochs=3,
            steps_per_epoch=steps_per_epoch,
            validation_steps=validation_steps,
            validation_data=valid_dataset
        )

        # freeze the first 249 layers and unfreeze the rest:
        for layer in model.layers[:116]:
            layer.trainable = False
        
        for layer in model.layers[116:]:
            layer.trainable = True 

    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=[ 'accuracy', 'top_k_categorical_accuracy' ])

    callbacks = [
        EarlyStopping(patience=EARLY_STOP_PATIENCE, verbose=1),
        ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=10, min_lr=0.001, verbose=1),
        ModelCheckpoint(OUTPUT_MODEL_LOCATION, verbose=1, save_best_only=True, save_weights_only=False),
        tf.keras.callbacks.CSVLogger(OUTPUT_HISTORY_LOCATION, separator=",", append=True),
        tf.keras.callbacks.Tensorboard(TB_LOG_LOCATION, update_freq=1)
    ]

    model.fit(train_dataset, epochs=EPOCHS,
        steps_per_epoch=steps_per_epoch,
        validation_steps=validation_steps,
        validation_data=valid_dataset,
        callbacks=callbacks)

    print('Training Complete!')

def evaluate_model(all_test_set):
    """
    EVALUATE THE MODEL ON THE TEST DATASET
    input (tf.dataset) - the test dataset ready for consumption
    """

    images = np.zeros(shape=(0, IMAGE_HEIGHT, IMAGE_WIDTH, CHANNELS))
    labels = np.zeros(shape=(0, len(CLASSES)))

    # load the model and get the predictions
    model = tf.keras.models.load_model('./Models/%s/model.h5' % MODELNAME, compile=False)
    predictions = model.predict(all_test_set)

    # stack all the images and classifications together
    for idx, (image_batch, label_batch) in enumerate(all_test_set) :
        images = np.vstack((images, image_batch))
        labels = np.vstack((labels, label_batch))

    # create accuracy/loss chart
    ax = plt.gca()
    df = pd.read_csv(OUTPUT_HISTORY_LOCATION)
    df.plot(x='epoch', y='accuracy', ax=ax)
    df.plot(x='epoch', y='loss', ax=ax)
    plt.show()

    # create ROC chart
    rocForClasses = []
    for idx in range(len(CLASSES)) :
        rocForClasses.append(roc_curve(labels[:,idx], predictions[:,idx]))

    plt.figure()
    lw = 2
    colours = ['darkorange', 'lightblue', 'lightgreen', 'gray']
    for idx, rocForClass in enumerate(rocForClasses) :
        plt.plot(rocForClass[0], rocForClass[1], color=colours[idx], label=CLASSES[idx])

    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic %s' % MODELNAME)
    plt.legend(loc="lower right")
    plt.show()

    predictions = np.argmax(predictions, axis=1)
    labels = np.argmax(labels, axis=1)

    # create confusion matrix
    labelsIdx = [0, 1, 2, 3]
    cm = confusion_matrix(labels, predictions, labels=labelsIdx)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    EvalUtils.createConfusionMatrix(cm, CLASSES)

    # create accuracy and jaccard score
    accuracyScore = accuracy_score(labels, predictions)
    jaccardScore = jaccard_score(labels, predictions, average='macro')
    print('accuracy score: ', round(accuracyScore, 2))
    print('jaccard score: ', round(jaccardScore, 2))    

    # create intersection over union and dice score
    (ioUList, diceList) = EvalUtils.getClassIOUAndDice(cm, CLASSES)
    print('IOU: ', ioUList)
    print('Dice Score: ', diceList)

    # precision, recall, f1-score and support
    cr = classification_report(labels, predictions, labels=labelsIdx, target_names = CLASSES)
    print(cr)

    print('EVALUATION COMPLETE!')

if __name__ == "__main__":
    
    try:
        if TRAIN_MODEL:
            # create the training dataset
            (train_dataset, valid_dataset) = DatasetUtils.create_train_val_sets(
                [(f"./DataTest/train/images/{x}/*") for x in ('Normal', 'Drusen', 'DME', 'CNV')],
                [ 0.7, 0.7, 0.7, 0.7 ]
            )

            train_dataset = DatasetUtils.prepare_for_network(train_dataset, BATCH_SIZE, process_path_class)
            valid_dataset = DatasetUtils.prepare_for_network(valid_dataset, BATCH_SIZE, process_path_class)
            do_training(train_dataset, valid_dataset)

        if EVALUATE_MODEL:
            # create the test datasest
            all_test_set = DatasetUtils.create_test_set([(f'./DataTest/test/images/{x}/*') for x in ('Normal', 'Drusen', 'DME', 'CNV')])
            all_test_set = all_test_set.map(process_path_class, num_parallel_calls=tf.data.experimental.AUTOTUNE).batch(BATCH_SIZE)
            evaluate_model(all_test_set)

    except NameError as ner:
        print(f'Error executing script: {ner}')
    except Exception as er:
        print(f'Error executing script: {er}')
