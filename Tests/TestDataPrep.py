import unittest
import sys
import os
# from os import listdir, getcwd
# from os.path import isfile, join

sys.path.append(os.path.join(os.getcwd(), 'Classification/Scripts'))

import DataPreparation

class TestDataPrep(unittest.TestCase):

    def test_onlyImages(self):
        imageDirs = [("./Classification/Data/train/images/%s" % x) for x in ('Normal', 'Drusen', 'DME', 'CNV')]
        for path in imageDirs :
            onlyfiles = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
            for file in onlyfiles :
                self.assertTrue(file.endswith('.png'))

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

    def test_isupper(self):
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)

if __name__ == '__main__':
    unittest.main()