import unittest
import sys
import os
from shutil import copyfile
from pathlib import Path

# from os import listdir, getcwd
# from os.path import isfile, join

sys.path.append(os.path.join(os.getcwd(), 'Classification/Scripts'))

import ModelPreparation as mp

TEST_DIR = os.path.join(os.getcwd(), str('Classification' + os.path.sep + 'Tests'))
TEST_MODEL_LOCATION = os.path.join(TEST_DIR, 'TEST_MODEL')
TEST_MODEL_NAME = 'testModel.h5'

class TestModelPrep(unittest.TestCase):
    def setUp(self):
        os.makedirs(TEST_MODEL_LOCATION, exist_ok=True)        

    def test_model_exists(self):
        '''test model exists'''

        modelPath = os.path.join(TEST_MODEL_LOCATION, TEST_MODEL_NAME)
        self.assertFalse(mp.modelExists(modelPath))
        
        # copy model file into location 
        src = os.path.join(TEST_DIR, TEST_MODEL_NAME)
        copyfile(src, modelPath)
        
        # test again
        self.assertTrue(mp.modelExists(modelPath))

    def test_model_retrieve(self):
        '''test model retrieval'''
        src = os.path.join(TEST_DIR, TEST_MODEL_NAME)
        model = mp.retrieveModel(src)
        self.assertIsNotNone(model)

    # def test_model_create(self):
    #     '''test model exists'''

    # def test_model_output_prepare(self):
    #     '''test model exists'''

    # def test_model_history_store(self):
    #     '''test model exists'''

    # def test_onlyImages(self):
    #     imageDirs = [("./Classification/Data/train/images/%s" % x) for x in ('Normal', 'Drusen', 'DME', 'CNV')]
    #     for path in imageDirs :
    #         onlyfiles = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    #         for file in onlyfiles :
    #             self.assertTrue(file.endswith('.png'))

    def tearDown(self):
        ''' fix tearDown '''
        # Path(TEST_MODEL_LOCATION, TEST_MODEL_NAME).unlink()
        Path(TEST_MODEL_LOCATION).rmdir()

if __name__ == '__main__':
    unittest.main()