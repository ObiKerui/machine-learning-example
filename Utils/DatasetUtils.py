import tensorflow as tf
from functools import reduce 

def get_size(dataset) :
    return dataset.reduce(0, lambda y,_: y + 1).numpy()

def create_train_val_sets(directories, splits) :
    '''
        CREATE THE TRAINING AND VALIDATION DATASETS
        Params:
        directories (list(str)) directories containing training/val data
        splits (list[float]) percentage to split the data between train/validation
    '''
    trainingSets = []
    validationSets = []

    datasets = list(map(lambda x : tf.data.Dataset.list_files(x), directories))
    sizes = list(map(lambda x :get_size(x), datasets))

    for idx, split in enumerate(splits) :
        dset = datasets[idx]
        train = dset.take(int(split * sizes[idx]))
        val = dset.skip(int(split * sizes[idx]))
        trainingSets.append(train)
        validationSets.append(val)
    
    trainSet = reduce(lambda a,b : a.concatenate(b), trainingSets)
    valSet = reduce(lambda a,b : a.concatenate(b), validationSets)

    return (trainSet, valSet)

def create_test_set(directories) :
    '''
        CREATE THE TEST DATASET
        Params:
        directories (list(str)) directories containing testset data
    '''
    datasets = list(map(lambda x : tf.data.Dataset.list_files(x), directories))
    sizes = list(map(lambda x :get_size(x), datasets))
    testSet = reduce(lambda a, b : a.concatenate(b), datasets)

    return (testSet)

def prepare_for_network(dataset, batchSize, processPath) :
    '''
        PREPARE THE DATASET FOR THE NETWORK 
        Params:
        dataset (tf.dataset): model data
        batchSize (int): batch size
        processPath (ftn): function to produce image/labels
    '''

    datasetSize = get_size(dataset)

    dataset = dataset.interleave(tf.data.Dataset.from_tensors, num_parallel_calls=tf.data.experimental.AUTOTUNE, cycle_length=2, block_length=2)
    dataset = dataset.cache()
    dataset = dataset.shuffle(datasetSize).repeat()
    dataset = dataset.map(processPath, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    dataset = dataset.batch(batchSize)
    dataset = dataset.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
    return dataset
