import json 
import os
import tensorflow as tf
from tensorflow.keras.layers import Input

''' PRE-TRAINED MODELS FOR BENCHMARKING ''' 
from tensorflow.keras.applications.xception import Xception
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.applications.resnet_v2 import ResNet50V2
from tensorflow.keras.applications.mobilenet_v2 import MobileNetV2
from tensorflow.keras.applications.densenet import DenseNet201
from tensorflow.keras.applications.inception_v3 import InceptionV3

def modelExists(modelLocation):
    return os.path.isfile(modelLocation)

def loadModel(modelNamePath, compile=False) :
    return tf.keras.models.load_model(modelNamePath, compile=compile)

def createModel(type, include_top=True, weights=None, input_tensor=Input(shape=(None, None, 3))) :
    '''
        CREATE A NEW MODEL FROM RANGE OF KERAS.APPLICATIONS MODELS
    '''

    if type == 'XCEPTION' : return Xception(include_top, weights, input_tensor)
    elif type == 'VGG16' : return VGG(include_top, weights, input_tensor)
    elif type == 'ResNet50V2' : return ResNet50V2(include_top, weights, input_tensor)
    elif type == 'MobileNetV2' : return MobileNetV2(include_top, weights, input_tensor)
    elif type == 'InceptionV3' : return InceptionV3(include_top, weights, input_tensor)

def prepareModelOutputLocation(modelNamePath) :
    '''
        MAKE OUTPUT DIR. IF IT DOES NOT EXIST
    '''
    os.makedirs(os.path.dirname(modelNamePath), exist_ok=True)

def storeModelHistory(modelName, history) :
    '''
        STORE THE MODEL HISTORY
    '''

    pathToSaveHistory = ('./Models/%s/History/%s.json' % (modelName, modelName.lower()))
    
    # make the directory structure if it does not exist
    os.makedirs(os.path.dirname(pathToSaveHistory), exist_ok=True)

    with open(pathToSaveHistory, 'w', encoding="utf-8") as outFile :
        jsonStr = json.dumps(str(history), ensure_ascii=False, indent=4)
        json.dump(jsonStr, outFile)
