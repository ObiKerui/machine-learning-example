from matplotlib import pyplot as plt
import json 
import ast
import numpy as np
from sklearn.metrics import confusion_matrix, roc_curve, auc, classification_report, accuracy_score, jaccard_score
import csv
import pandas as pd

def getClassIOUAndDice(cm, CLASSES):
    '''
        Computes Class Intersection Over Union and Dice score
        Params: ([[float]]), [string]
    '''
    nbrClasses = len(CLASSES)
    IoU = np.zeros(nbrClasses)
    Dice = np.zeros(nbrClasses)

    for j in range(nbrClasses):
        IoU[j] = cm[j,j] / (sum(cm[:,j])+sum(cm[j,:]) - cm[j,j])
        Dice[j] = 2 * IoU[j] / (1 + IoU[j])

    return (IoU, Dice)

def createConfusionMatrix(cmData, classes):
    '''
        CREATE CONFUSION MATRIX FROM CM-DATA
    '''
    fig, ax = plt.subplots(figsize=(10, 10))
    im = ax.imshow(cmData, interpolation='nearest', cmap=plt.cm.Blues)
    ax.figure.colorbar(im, ax=ax)

    nbrClasses = cmData.shape[0]
    ax.set_xticklabels(classes)
    ax.set_yticklabels(classes)
    ax.set_xticks(np.arange(nbrClasses))
    ax.set_yticks(np.arange(nbrClasses))

    # Loop over data dimensions and create text annotations.
    fmt = '.2f'
    thresh = cmData.max() / 2.

    for i in range(cmData.shape[0]):
        for j in range(cmData.shape[1]):
            ax.text(j, i, format(cmData[i, j], fmt), ha="center", va="center", color="white" if cmData[i, j] > thresh else "black")
  
    plt.show()


def showLabel(label, model):
    '''
        VISUALLY INSPECT THE PREDICTIONS
    '''
    (idx, (imageBatch, labelBatch)) = label
    prediction = model.predict(imageBatch)

    fig, axs = plt.subplots(1, 3, figsize=(20, 20))
    axs[0].set_title('image')
    axs[0].imshow(imageBatch[0])
    axs[1].set_title('prediction')
    axs[1].imshow(prediction[0,:,:,0])
    axs[2].set_title('truth')
    axs[2].imshow(labelBatch[0,:,:,0])

    plt.show()

def compareResults(predictions, images, labels):
    '''
        VISUALLY INSPECT THE PREDICTIONS
    '''
    fig, axs = plt.subplots(1, 3, figsize=(20, 20))
    axs[0].set_title('prediction')
    axs[0].imshow(predictions[0,:,:,0])
    axs[1].set_title('segmentation (truth)')    
    axs[1].imshow(labels[0,:,:,0])
    axs[2].set_title('image')        
    axs[2].imshow(images[0])    
    plt.show()
