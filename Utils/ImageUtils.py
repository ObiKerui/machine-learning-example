import tensorflow as tf
import numpy as np 

def load_image(file_path, height, width, channels = 1, normalise = False):
    '''
        LOAD IMAGE FROM FILEPATH
        params:
            file_path (string) : path to image file
            height, width, channels (int, int, int): required dimensions of image
            normalise: (boolean): perform normalisation of the image
        returns:
            image data array
    '''
    img = tf.io.read_file(file_path)
    img = tf.image.decode_png(img, channels=channels)
    img = tf.image.convert_image_dtype(img, tf.float32) if normalise else img
    img = tf.image.resize_with_pad(img, height, width)  
    return img

def invert_mask(mask):
    '''
        INVERT THE PIXELS OF THE MASK
        params:
            mask ([[[float]]]) : mask image data
        returns:
            mask ([[[float]]])
    '''

    condition = tf.math.greater(mask, 0)
    ones = tf.ones(mask.shape)
    sub = tf.subtract(ones, mask)
    result = tf.where(condition, sub, mask)
    return result

def augmentImage(image, dims, rand) :
    '''
        PERFORM IMAGE AUGMENTATIONS
        params:
            image ([[[float]]]) : image data
            dims: dimensionso to crop to
            rand: float: random number
        returns:
            image ([[[float]]])
    '''

    isEven = True if tf.equal(rand % 2, 0) else False
    scales = [ 1.0, 0.9, 0.8, 0.7, 0.6 ]

    # flip the image
    if isEven :
        image = tf.image.flip_left_right(image)

    # rotate the image
    image = tf.image.rot90(image, rand)
    
    # colour the image (change to adjust) 
    image = tf.image.random_hue(image, 0.2)
    image = tf.image.random_saturation(image, 0.5, 1.0)
    image = tf.image.random_brightness(image, 0.2)
    image = tf.image.random_contrast(image, 0.5, 1.0)

    # zoom / crop the image
    boxes = np.zeros((len(scales), 4))

    for idx, scale in enumerate(scales) :
        x1 = y1 = 0.5 - (0.5 * scale)
        x2 = y2 = 0.5 + (0.5 * scale)
        boxes[idx] = [ x1, y1, x2, y2 ]

    croppedImages = tf.image.crop_and_resize([image], boxes=boxes, box_indices=np.zeros(len(scales)), crop_size=dims)
    idx = rand % len(croppedImages)
    image = croppedImages[idx]

    return image
